<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
        public function index(){

        $data['articles'] = Article::all();
        return view('articles/show', $data);

    }

    public function show(Article $article){

        return view('articles/show_one', compact('article'));
    }
}
