<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
        public function index(){

        $data['products'] = Product::all();
        return view('products/show', $data);

    }

    public function show(Product $product){

        return view('products/show_one', compact('product'));
    }
}
