<?php

namespace App\Http\Controllers;

use App\Tour;
use Illuminate\Http\Request;

class ToursController extends Controller
{
        public function index(){

        $data['tours'] = Tour::all();
        return view('tours/show', $data);

    }

    public function show(Tour $tour){

        return view('tours/show_one', compact('tour'));
    }
}
