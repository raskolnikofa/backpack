<?php

use Illuminate\Database\Seeder;

class TablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('articles')->insert([
            [
                'title' => 'These Are The Most Colourful Streets in Europe',
                'alias' => 'the_most_colourful_city',
                'intro' => 'From the prettiest towns and remote villages to unusual buildings and fisherman’s houses, Europe is a treasure trove of colourful locations just waiting to be discovered. Here are just some of our favourite streets that are bursting with colour.',
                'text' => 'Painted by American artist James Rizzi in his distinctive graphic illustrative style, the Happy Rizzi Haus may have divided opinion between the young and old residents of Braunschweig, but it’s certainly brought colour to Ackerhof. Designed by Konrad Kloster, the office complex incorporates misshapen windows and reliefs that mimic Rizzi’s drawing style. The Old Market Square of Poznań in Poland is surrounded by former merchants’ houses that have been painted in Mediterranean hues of azul blue, verdant green, and earthy yellows. Much of the square was destroyed during World War II and was carefully rebuilt in the 1950s, so each building is individual with different ornate details and friezes.',
                'author' => 'Freire Barnes',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
 			[
                'title' => 'The Best Towns and Cities in Spain to Visit in 2017',
                'alias' => 'the_best_spain_2017',
                'intro' => 'Just face it, every year is a good one to visit Spain, with the wonderful Mediterranean climate, historic cities, beautiful beaches and fascinating culture. We’ve found a few cities with anniversary celebrations, exciting festivals and even an underwater museum, making them particularly worth a visit this year.',
                'text' => 'Every year the Spanish city of Valencia holds its famous Las Fallas festival. During the festival, large and elaborate paper mâché sculptures are displayed all over the city, then burnt on the last night. There are also daily firework displays, cultural parades and live bands throughout the city. This year it will be held from March 15 – 19. The pretty town of Yaiza, on the south coast of the Canary Island of Lanzarote has become home to Europe’s first underwater museum, which opened at the beginning of this year. Why not be one of the first to visit Museo Atlántico? It is the brainchild of British artist Jason deCaires Taylor, who spent two years creating sculptures to be sunk in the Coloradas Bay. They can be seen on a 12-15 metre dive or by glass-bottomed boat.',
                'author' => 'Esme Fox',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
  			[
                'title' => 'This Incredible New Library Is the Best Spot for Book Lovers Ever',
                'alias' => 'the_incredible_new_library',
                'intro' => 'Designed by Rotterdam-based, powerhouse Dutch design firm MVRDV, the 33,700-square-metre Tianjin Binhai Public Library is a feast for the eyes as much as for the soul. ',
                'text' => 'Commissioned as part of a new cultural district in the Chinese coastal city, the atrium of the building has been built to look like a giant 3D eyeball, staring out into the street through the glazed glass facade. The library, which miraculously took only three years to build from first sketch to site opening, is a whopping five storeys tall and is entirely covered in floor-to-ceiling bookshelves swirling and curving across various levels, to form the shape of an eye-socket. The pupil of the eye is formed by what looks like glowing disco ball in the centre of one of the rooms. The curved bookshelves provide many sleek nooks and crannies in which to cuddle up with your favourite book. Stairways also snake their way up through the shelves, giving visitors access to the different floors.',
                'author' => 'India Irving',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
       
        ]);

		DB::table('tours')->insert([
            [
                'title' => 'Winter Getaway',
                'alias' => 'winter_getaway',
                'price' => '980',
                'description' => 'Start and end in London! With the discovery tour Winter Getaway, you have a 18 day tour package taking you through London, England and 15 other destinations in Europe. Winter Getaway includes accommodation in a hotel and hostel as well as an expert guide, meals, transport and more.',
                'img' => 'https://c1.staticflickr.com/9/8211/8304139037_48c033dc07_b.jpg',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'title' => 'Balkan Trail',
                'alias' => 'balkan_trail',
                'price' => '615',
                'description' => 'Start in Athens and end in Split! With the in-depth cultural tour Balkan Trail, you have a 9 day tour package taking you through Athens, Greece and 9 other destinations in Europe. Balkan Trail includes transport.',
                'img' => 'http://www.balkanholidays.co.uk/production/images/resorts/Bulgaria/Winter/Bansko/600x380_01.jpg',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'title' => '7 day The Grand Tour of Ireland (small group tour)',
                'alias' => '7_days_ireland_small tour',
                'price' => '1100',
                'description' => 'Start and end in Dublin! With the discovery tour 7 day The Grand Tour of Ireland (small group tour), you have a 7 day tour package taking you through Dublin, Ireland and 9 other destinations in Ireland. 7 day The Grand Tour of Ireland (small group tour) is a small group tour that includes accommodation as well as an expert guide, transport and more.',
                'img' => 'https://www.transat.com/getmedia/a0538290-1cf8-4cec-ba22-95aec079d44c/Ireland-Couples-1000x604.jpg.aspx',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
       
        ]);
		DB::table('products')->insert([
            [
                'title' => 'The North Face Backpack',
                'alias' => 'north_face_backpack',
                'price' => '130',
                'description' => 'The best backpack for our tours',
                'img' => 'https://images.thenorthface.com/is/image/TheNorthFace/CHJ9_WAB_hero?$638x745$',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'title' => 'Danner Boots',
                'alias' => 'danner_boots',
                'price' => '400',
                'description' => 'The best boots for our tours',
                'img' => 'https://s-media-cache-ak0.pinimg.com/564x/e6/ab/aa/e6abaaafc99934ec27e9f40f7495f715.jpg',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'title' => 'Stels Navigator 500',
                'alias' => 'stels_navigator_500',
                'price' => '2540',
                'description' => 'The best Stels for our tours',
                'img' => 'https://veloolimp.com/images/product_images/original_images/2017/v_14738_0.jpg',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],

       
        ]);

    }
}
