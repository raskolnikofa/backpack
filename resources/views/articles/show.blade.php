@extends('layouts.app')

@section('content')
<div class="container">
<div class="row">
        @foreach($articles as $article)
            <div class="col-md-4">
                <h2> {{ $article->title }} </h2>
                <p>
                    {{ $article->intro }}
                </p>
                <a href="/articles/{{$article->alias}}" class="btn btn-default">Show more</a>
            </div>
        @endforeach


    </div>
</div>
@endsection
