@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-8 col-offset-2">
                <h1>{{ $article->title }}</h1>
                <p>{{ $article->text }}</p>
                <p>{{ $article->author }}</p>
                <p>{{ $article->created_at }}</p>
            </div>
</div>
@endsection