@extends('layouts.app')

@section('content')
<div class="container">
<div class="row">
        @foreach($products as $product)
            <div class="col-md-4">
                <h2> {{ $product->title }} </h2>
                <p>
                      <img src="{{ $product->img }}">
                </p>
                <p>
                    {{ $product->price }}
                </p>
                <a href="/stuff/{{$product->alias}}" class="btn btn-default">Show more</a>
            </div>
        @endforeach


    </div>
</div>
@endsection