@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-8 col-offset-2">
                <h1>{{ $product->title }}</h1>
                <p>                      <img src="{{ $product->img }}"></p>
                <p>{{ $product->price }}</p>
                <p>{{ $product->description }}</p>
            </div>
</div>
@endsection