@extends('layouts.app')

@section('content')
<div class="container">
<div class="row">
        @foreach($tours as $tour)
            <div class="col-md-4">
                <h2> {{ $tour->title }} </h2>
                <p>
                      <img src="{{ $tour->img }}">
                </p>
                <a href="/tours/{{$tour->alias}}" class="btn btn-default">Show more</a>
            </div>
        @endforeach


    </div>
</div>
@endsection