@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-8 col-offset-2">
                <h1>{{ $tour->title }}</h1>
                <p><img src="{{ $tour->img }}"></p>
                <p>{{ $tour->price }}</p>
                <p>{{ $tour->description }}</p>
            </div>
</div>
@endsection