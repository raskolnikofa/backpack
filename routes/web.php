<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//articles

Route::get('/articles', 'ArticlesController@index');

Route::get('/articles/{article}', 'ArticlesController@show');

//tours

Route::get('/tours', 'ToursController@index');

Route::get('/tours/{tour}', 'ToursController@show');

//product

Route::get('/stuff', 'ProductsController@index');

Route::get('/stuff/{product}', 'ProductsController@show');